package core;
import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Ajay Kumar
 * CREATED ON 2/16/2019
 */
public class FileUtil {
    private static final Logger logger = Logger.getLogger(FileUtil.class.getName());

    public static List objectReader(String fileName){
        String line;
        List<Record> list = new ArrayList<>();
        //try-with-resource reading from classpath
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new ClassPathResource(fileName).getInputStream()))) {
            while ((line = br.readLine()) != null){
                String[] inputLine = line.split(";");
                Record obj = new Record(inputLine[0].trim(), inputLine[1].trim(),
                        new SimpleDateFormat("dd-MMM-yyyy").parse(inputLine[2]), Double.parseDouble(inputLine[3].trim()));
                list.add(obj);
            }
        }
        catch(IOException fnfe) {
            logger.severe("Input Files not present, program exiting");
            System.exit(-1);
        } catch (ParseException e) {
            logger.severe("Invalid Date format in file");
        }
        return list;
    }
}

