package core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.logging.Logger;

/**
 * @author I686340
 * CREATED ON 2/24/2019\
 * This controller triggers the Reconcilitation Service which prints the report on console, it can be passed back to browser using html pages and
 * thymaleaf dependecies which I haven't used in this program
 */
@Controller
public class SCController {
    private static final Logger logger = Logger.getLogger(SCController.class.getName());

    @Autowired
    ReconciliationService service;

    @RequestMapping(value = "/getReconReport", produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public String getReconReport(){
        logger.info("running controller");
        service.printReport();
        logger.info("report printed on console");
        return "Result Printed on console";
    }
}
