package main.java;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Ajay Kumar
 * CREATED ON 2/16/2019
 */
public class Record implements Serializable {

    private static final long serialVersionUID = 1L;

    private String transactionId;
    private String accountId;
    private Date postingDate;
    private double amoount;

    public Record(String transactionId, String accountId, Date postingDate, double amoount) {
        this.transactionId = transactionId;
        this.accountId = accountId;
        this.postingDate = postingDate;
        this.amoount = amoount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Date getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(Date postingDate) {
        this.postingDate = postingDate;
    }

    public double getAmoount() {
        return amoount;
    }

    public void setAmoount(double amoount) {
        this.amoount = amoount;
    }

    @Override
    public boolean equals(Object rec){
        Record recY = (Record) rec;
        return (this.getAccountId().equalsIgnoreCase(recY.getAccountId()) &&
                this.getPostingDate().equals(recY.getPostingDate()) &&
                this.getAmoount() == recY.getAmoount());
    }
}
