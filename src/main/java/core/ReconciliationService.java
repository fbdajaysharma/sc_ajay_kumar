package core;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Ajay Kumar
 * CREATED ON 2/16/2019
 *
 * This program assumes files are in resources folder. It may be read from some where else as well.
 * Assumption is record structure might change but separator would remain constant
 * File names are assumed but it can be passes as command line arguments or from controller
 */
@Service
public class ReconciliationService {

    private static final Logger logger = Logger.getLogger(ReconciliationService.class.getName());
    public void printReport(){
        logger.info("Started recon");
        //getting file name as argument
        List<Record> listX = FileUtil.objectReader("X.txt");
        List<Record> listY = FileUtil.objectReader("Y.txt");
        List<String> exactMatch = new ArrayList<>();
        List<String> weakMatch = new ArrayList<>();
        List<String> breakX = new ArrayList<>();
        List<String> breakY = new ArrayList<>();
        List<List<String>> listFinal = new ArrayList<>();
        //start comapring
        for(int i=0; i< listX.size(); i++){
            Record recX = listX.get(i);
                Record recY = listY.get(i);
                if(recX.equals(recY)){
                    exactMatch.add(recX.getTransactionId()+recY.getTransactionId());
                }
                else if(checkWeakMatch(recX ,recY)){
                    weakMatch.add(recX.getTransactionId()+recY.getTransactionId());
                }
                else{
                    breakX.add(recX.getTransactionId());
                    breakY.add(recY.getTransactionId());
                }
        }
        //add elements to final list
        listFinal.add(exactMatch);
        listFinal.add(weakMatch);
        listFinal.add(breakX);
        listFinal.add(breakY);
        printReport(listFinal);
    }

    private boolean checkWeakMatch(Record recX , Record recY){
        boolean result = false;
        if(recX.getAccountId().equalsIgnoreCase(recY.getAccountId()) &&
                getWorkingDays(recX.getPostingDate(), recY.getPostingDate()) <= 1 &&
                    Math.abs(recX.getAmoount() - recY.getAmoount()) <= 1e-1){
            result = true;
        }
        return result;
    }

    private void printReport(List<List<String>> listFinal){
        String[] template = {"# XY exact matches", "# XY weak matches", "# X breaks", "# Y breaks"};
        int i = 0;
        for(List<String> listPrint: listFinal){
            System.out.println(template[i]);
            for(String out: listPrint){
                System.out.print(out + ", ");
            }
            //introduce blank line
            System.out.println("");
            i++;
        }
    }

    //using java 8 time api features
    private static long getWorkingDays(final Date dateX, final Date dateY) {

        LocalDate localDateX = dateX.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate localDateY = dateY.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        final DayOfWeek startW = localDateX.getDayOfWeek();
        final DayOfWeek endW = localDateY.getDayOfWeek();

        final long days = ChronoUnit.DAYS.between(localDateX, localDateY);
        final long daysWithoutWeekends = days - 2 * ((days + startW.getValue())/7);

        //adjust for starting and ending on a Sunday:
        return daysWithoutWeekends + (startW == DayOfWeek.SUNDAY ? 1 : 0) + (endW == DayOfWeek.SUNDAY ? 1 : 0);
    }
}