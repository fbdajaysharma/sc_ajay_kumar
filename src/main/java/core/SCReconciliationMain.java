package core;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Logger;

/**
 * @author I686340
 * CREATED ON 2/24/2019
 */

@SpringBootApplication
public class SCReconciliationMain {

    private static final Logger logger = Logger.getLogger(SCReconciliationMain.class.getName());

    public static void main(String[] args){
        logger.info("starting application");
        SpringApplication.run(SCReconciliationMain.class, args);
    }
}
