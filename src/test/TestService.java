import core.FileUtil;
import core.ReconciliationService;
import core.Record;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author I686340
 * CREATED ON 2/17/2019
 */
@RunWith(PowerMockRunner.class)
public class TestService {

    private MockMvc mockMvc;

    @InjectMocks
    private ReconciliationService service;

    @Mock
    private FileUtil fileUtil;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(service).build();
    }

    @PrepareForTest({FileUtil.class})
    @Test
    public void printReport() throws Exception {
        List list = new ArrayList();
        Record record = new Record("x0", "102", new Date(), 900);
        list.add(record);

        //mocking static method to read file in FileUtil
        PowerMockito.mockStatic(FileUtil.class);
        PowerMockito.when(FileUtil.objectReader("X.txt")).thenReturn(list);
        PowerMockito.when(FileUtil.objectReader("Y.txt")).thenReturn(list);
        //mocking private methods
        service.printReport();
    }

    @After
    public void tearDown() throws Exception {
        //do clean up as needed
    }
}
