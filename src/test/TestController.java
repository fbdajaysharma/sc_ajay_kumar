import core.ReconciliationService;
import core.SCController;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

/**
 * @author I686340
 * CREATED ON 2/17/2019
 */
@WebAppConfiguration
public class TestController {

    private MockMvc mockMvc;

    @InjectMocks
    private SCController controller;

    @Mock
        private ReconciliationService service;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void getReconReport() throws Exception {
        doNothing().when(service).printReport();
        ResultActions resultActions = mockMvc.perform(get("/getReconReport"));
        resultActions.andExpect(MockMvcResultMatchers.status().isOk());
    }

    @After
    public void tearDown() throws Exception {
        //do clean up as needed
    }
}
